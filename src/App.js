import './App.css';
import './components/taskInputcss.css'
import {useEffect, useState} from "react";
import {AiOutlineDelete} from "react-icons/ai";
import {BsCheckLg} from "react-icons/bs";
import {CiEdit} from "react-icons/ci";

function App() {
    const [isCompleted,setIscompleted]=useState(true);
    const [allTodos,setAllTodos]=useState([]);
    const [title,setTitle]=useState("");
    const [decription,setDescription]=useState("");
    const [completed,setCompleted]=useState([]);
    const [selectedTaskIndex, setSelectedTaskIndex] = useState(null);

    const addToDo=()=>{
        const date = new Date ();
        var dd = date.getDate ();
        var mm = date.getMonth () + 1;
        var yyyy = date.getFullYear ();
        var hh = date.getHours ();
        var minutes = date.getMinutes ();
        var finalDate = dd + '-' + mm + '-' + yyyy + ' at ' + hh + ':' + minutes;
        let newItem={
            title:title,
            description:decription,
            completedOn:finalDate

        }
        let updatedAllTodo=[...allTodos];
        if (selectedTaskIndex !== null) {
            // If selectedTaskIndex is not null, it means we're updating an existing task
            updatedAllTodo[selectedTaskIndex] = newItem;
            setSelectedTaskIndex(null); // Reset selected task index after update
        } else {
            // If selectedTaskIndex is null, it means we're adding a new task
            updatedAllTodo.push(newItem);
        }
        setAllTodos(updatedAllTodo);
        localStorage.setItem('todolist',JSON.stringify(updatedAllTodo));
        setTitle("");
        setDescription("");
    };


    const deleteToDo = (index) => {

        let updatedAllTodo = [...allTodos];
        updatedAllTodo.splice(index,1);

        setAllTodos(updatedAllTodo);
        localStorage.setItem('todolist', JSON.stringify(updatedAllTodo));
    };
    const deleteCompleted = (index) => {

        let updatedAllTodo = [...completed];
        updatedAllTodo.splice(index,1);

        setCompleted(updatedAllTodo);
        localStorage.setItem('completedToDos', JSON.stringify(updatedAllTodo));
    };
    const handlecomplete=(index)=>{
        const date = new Date ();
        var dd = date.getDate ();
        var mm = date.getMonth () + 1;
        var yyyy = date.getFullYear ();
        var hh = date.getHours ();
        var minutes = date.getMinutes ();
        var finalDate = dd + '-' + mm + '-' + yyyy + ' at ' + hh + ':' + minutes;

        let filteredTodo = {
            ...allTodos[index],
            completedOn: finalDate,
        };

        // console.log (filteredTodo);

        let updatedCompletedList = [...completed, filteredTodo];
        // console.log (updatedCompletedList);
        setCompleted (updatedCompletedList);
        localStorage.setItem (
            'completedTodos',
            JSON.stringify (updatedCompletedList)
        );
        // console.log (index);

        deleteToDo(index);
    }
    const editToDo = (index) => {
        setSelectedTaskIndex(index);
        const selectedTask = allTodos[index];
        setTitle(selectedTask.title);
        setDescription(selectedTask.description);
    };

    useEffect(() => {
        let saveTodos=JSON.parse( localStorage.getItem("todolist"));
        if(saveTodos){
            setAllTodos(saveTodos);
        }
        let saveCompleted=JSON.parse( localStorage.getItem("completedTodos"));
        if(saveCompleted){
            setCompleted(saveCompleted);
        }
    }, []);

  return (
    <div className="App">
      <h1>My to-do</h1>
        <div className='todo-wrapper'>
            <div className="todo-input">
                <div className="todo-input-item">
                    <label>Title</label>
                    <input type="text" value={title} onChange={(e)=>setTitle(e.target.value)} placeholder="title"/>
                </div>
                <div className="todo-input-item">
                    <label>Description</label>
                    <input type="text" value={decription} onChange={(e)=>setDescription(e.target.value)} placeholder="description"/>
                </div>
                <div className="todo-input-item">

                    <button type="button" onClick={addToDo} className="primaryBtn">Add</button>
                </div>
            </div>
            <div className="two-Btn">

                <button className={`Btn-Btn ${isCompleted===false && 'active'}`} onClick={()=>setIscompleted(true)}>Todo</button>
                <button className={`Btn-Btn ${isCompleted===true && 'active'}`} onClick={()=>setIscompleted(false)} >Completed</button>
            </div>
            <div className="todo-list">

                {isCompleted===false && completed.map((item,index)=>{
                    return (
                        <div className="Todo-list-item" key={index}>
                            <div>
                                <h3>{item.title}</h3>
                                <p>{item.description}</p>
                                <p><small>{item.completedOn}</small></p>
                            </div>
                            <div>
                                <AiOutlineDelete className="icon" onClick={()=>deleteCompleted(index)}/>
                                {/*<BsCheckLg className="check-icon" onClick={()=>deleteCompleted(index)}/>*/}

                            </div>


                        </div>
                    )
                })}
                {isCompleted===true && allTodos.map((item,index)=>{
                return (
                <div className="Todo-list-item" key={index}>
                    <div>
                        <h3>{item.title}</h3>
                        <p>{item.description}</p>
                        <p><small>{item.completedOn}</small></p>
                    </div>
                    <div>
                    <AiOutlineDelete className="icon" onClick={()=>deleteToDo(index)}/>
                    <BsCheckLg className="check-icon" onClick={()=>handlecomplete(index)}/>
                    <CiEdit className="edit-icon" onClick={()=>editToDo(index)} />

                </div>


            </div>
            )
            })}

            </div>


        </div>


    </div>
  );
}

export default App;
